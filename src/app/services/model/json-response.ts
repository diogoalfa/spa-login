export class JsonResponse<T> {
    private _status:string;
    private _codeStatus:number;
    private _data:T;


  get status(): string {
    return this._status;
  }

  set status(value: string) {
    this._status = value;
  }

  get codeStatus(): number {
    return this._codeStatus;
  }

  set codeStatus(value: number) {
    this._codeStatus = value;
  }


  get data(): T {
    return this._data;
  }

  set data(value: T) {
    this._data = value;
  }
}
