import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {JsonResponse} from './model/json-response';
import {catchError} from 'rxjs/operators';
import {UserLogin} from '../core/model/user-login';
import {TokenOauth} from '../core/model/token-oauth';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private static readonly  urlApiUser: string = environment.apiEndpointUser;
  private static readonly  urlApiAuthServer: string = environment.apiAuthServerToken;
  private static readonly  clientId: string = environment.apiAuthClientId;
  private static readonly  clientSecret: string = environment.apiAuthClientSecret;

  constructor(private http: HttpClient) { }

  public getUser(username: string, password: string): Observable<any> {
    const METHOD  = '/user';
    const URL     = LoginService.urlApiUser;
    const Http    = this.http.get<any>(URL);
    return Http.pipe(
          catchError(error => {
              return throwError(error);
          })
      );
  }

  public getSession(username: string, password: string): Observable<TokenOauth> {
      const body = new HttpParams()
          .set('username', username)
          .set('password', password)
          .set('grant_type', 'password');
      const URL     = LoginService.urlApiAuthServer;
      const Http    = this.http.post<TokenOauth>(
          URL,
          body.toString(),
          {
              headers: new HttpHeaders()
                  .set('Content-Type', 'application/x-www-form-urlencoded')
                  .set('Authorization', 'Basic ' + btoa(LoginService.clientId + ':' + LoginService.clientSecret))
            }
          );
      return Http.pipe(
          catchError(error => {
              return throwError(error);
          })
      );
  }


    public changePass(username: string, password: string, newPassword: string): Observable<JsonResponse<string>> {
        const URL     = LoginService.urlApiUser;
        const METHOD  = '/user/password';
        const Http    = this.http.post<JsonResponse<string>>(
            URL  + METHOD, {username, password, newPassword}
        );
        return Http.pipe(
            catchError(error => {
                return throwError(error);
            })
        );
    }

}
