import { NgModule }                from '@angular/core';
import { CommonModule }             from '@angular/common';
import { NotificationsComponent }   from './notifications/notifications.component';
import { MatIconModule, MatDialogModule, MatButtonModule } from '@angular/material';

@NgModule({
  declarations: [
    NotificationsComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule
  ],
  exports: [
    NotificationsComponent
  ],
  entryComponents: [
  ]
})
export class ComponentsModule { }
