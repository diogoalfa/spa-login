import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import session from '../../../../core/model/session';

@Component({
    selector: 'app-topnav',
    templateUrl: './topnav.component.html',
    styleUrls: ['./topnav.component.scss']
})
export class TopnavComponent implements OnInit {
    public pushRightClass: string;
    public user: string;

    constructor(
        public  router: Router
    ) {
        this.router.events.subscribe(val => {
            if (val instanceof NavigationEnd && window.innerWidth <= 992 && this.isToggled()) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {
        this.pushRightClass   = 'push-right';
        this.getUser();
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    getUser(): void {
        this.user = session.userName || '';
    }

    onLoggedout() {
        const vm = this;
        const cleanUserInfo = () => {
            session.reset();
        }
        this.router.navigate(['/login']);
    }

}
