import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-loading-dialog',
  templateUrl: './loading-dialog.component.html',
  styleUrls: ['./loading-dialog.component.scss']
})
export class LoadingDialogComponent {

  loadingText = 'Cargando...';

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    if (data) {
      this.loadingText = data.msg;
    }
  }

}
