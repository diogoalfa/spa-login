import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../../services/login.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import session from '../../../core/model/session';
import {LoadingDialogComponent} from '../components/loading-dialog/loading-dialog.component';
import {MatDialog} from '@angular/material';
import {Router} from "@angular/router";


@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    loading: boolean;
    loadingDashboard: boolean;
    disableSubmit = false;

    changePassForm = new FormGroup({
        oldPass: new FormControl('', Validators.required),
        newPassword: new FormControl('', Validators.required),
        newPasswordConfirm: new FormControl('', Validators.required)
    });

    constructor(
        private ls: LoginService,
        public dialog: MatDialog,
        private router: Router) {
        this.loadingDashboard = false;
    }

    ngOnInit() {
        this.loadingDashboard = false;
        this.loading = false;
    }

    onSubmit() {
        this.loading = false;
        this.disableSubmit = true;
        const password = this.changePassForm.controls.oldPass.value;
        const newPassword = this.changePassForm.controls.newPassword.value;
        const newPasswordConfirm = this.changePassForm.controls.newPasswordConfirm.value;
        this.ls.changePass(session.userName, password, newPassword)
            .subscribe(response => {
                if (response.data === 'ok') {
                    this.router.navigate(['/login'], { queryParamsHandling: 'preserve' });
                } else {
                    alert('error al cambiar de contraseña');
                }
            }, error => {
                console.log('error change pass');
            });
    }

}
