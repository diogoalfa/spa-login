import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {LoadingDialogComponent} from '../layout/components/loading-dialog/loading-dialog.component';
import {LoginService} from '../../services/login.service';
import {UserLogin} from '../../core/model/user-login';
import {HttpParams} from '@angular/common/http';
import session from '../../core/model/session';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  userForm = new FormGroup({
    userName: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });
  disableSubmit = false;

  constructor(
    private router: Router,
    public dialog: MatDialog,
    private ls: LoginService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.disableSubmit = true;
    const username = this.userForm.controls.userName.value;
    const pass = this.userForm.controls.password.value;
    this.ls.getSession(username, pass).subscribe(response => {
        session.init(response.access_token, username, 'user');
        localStorage.setItem('app-login', document.referrer);
        console.log('session init');
        console.log(response);
        this.router.navigate(['/dashboard'], { queryParamsHandling: 'preserve' });
      }, error => {
        alert(error.error.error_description);
      });
  }

}
