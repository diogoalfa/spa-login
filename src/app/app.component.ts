import {Component, OnInit} from '@angular/core';
import session from './core/model/session';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: []
})
export class AppComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit() {
      if (!session.isLoggedIn() ) {
          this.router.navigate(['/login'], { queryParamsHandling: 'preserve' });
      }
  }
}
