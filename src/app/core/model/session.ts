const SIX_HOURS_IN_MS = 1000 * 60 * 60 * 6;
const EXPIRATION_MS = SIX_HOURS_IN_MS;

class Session {
    private ts: string;
    private exp = 0;
    private rol: string;
    private username: string;

    public isExpired() {
        return this.exp < new Date().getTime();
    }

    public isLoggedIn() {
        return this.ts && !this.isExpired();
    }

    public reset() {
        this.exp = 0;
        delete this.username;
        delete this.rol;
    }

    public init(ts: string, user: string, rol: string) {
        this.ts = ts;
        this.exp = new Date().getTime() + EXPIRATION_MS;
        this.username = user;
        this.rol = rol;
    }

    public init(ts: string, user: string, rol: string, expired: number) {
        this.ts = ts;
        this.exp = expired;
        this.username = user;
        this.rol = rol;
    }

    public get userRol() {
        return this.rol;
    }

    public get userName() {
        return this.username;
    }

    public get token() {
        return this.ts;
    }

}

export default new Session();
